FROM php:7.3-fpm
MAINTAINER alex-saf

ENV LANG=en_US.UTF-8 LANGUAGE=en_US:en LC_ALL=en_US.UTF-8 DEBIAN_FRONTEND=noninteractive

RUN apt-get update \
    && apt-get -y install \
            libicu-dev \
            zlib1g-dev \
            libzip-dev \
            libssl-dev \
            libxml2-dev \
            curl libcurl4 libcurl4-gnutls-dev \
            libxslt-dev \
            libjpeg62-turbo-dev \
            libpng-dev libfreetype6-dev \
    #        --no-install-recommends \

    # Required extension
    && docker-php-ext-install -j$(nproc) intl \

    # Additional common extensions
    && docker-php-ext-install -j$(nproc) opcache \
    && docker-php-ext-install bcmath \
    && docker-php-ext-install mysqli && docker-php-ext-install pdo_mysql \
    && docker-php-ext-install mbstring \
    && docker-php-ext-install gd \
    && docker-php-ext-install curl \
    && docker-php-ext-install phar \
    && docker-php-ext-install json \
    && docker-php-ext-install xml \
    && docker-php-ext-install xsl \
    && docker-php-ext-install iconv \
    && docker-php-ext-install simplexml \
    && CFLAGS="-I/usr/src/php" docker-php-ext-install xmlreader \
    
    # Required by composer
    && docker-php-ext-install -j$(nproc) zip \

    # Cleanup to keep the images size small
    && apt-get purge -y \
        libicu-dev \
        zlib1g-dev \
    && apt-get autoremove -y \
    && rm -r /var/lib/apt/lists/*

#
